package 结构型模式.装饰;

public interface I_Human {
	String name = "human";
	int legnum = 2;
	int armnum = 2;
	
	void wearClothes();
	void walkToWhere();
}
