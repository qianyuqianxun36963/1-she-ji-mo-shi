package 结构型模式.桥接;

/**
 *  The Implementor
 */
public interface I_Text  {
    public abstract void DrawTextImp();
}