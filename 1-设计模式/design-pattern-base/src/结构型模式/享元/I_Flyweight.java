package 结构型模式.享元;
/**
 *  A FlyWeight
 */
public interface I_Flyweight  {
    public abstract void SetFont(String color, int size);
    public abstract void GetFont();
}