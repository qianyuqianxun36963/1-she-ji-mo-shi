package 创建型模式.工厂模式;

public abstract class Factory_Abstract {
	abstract I_Moveable getMoveable();
	abstract I_Writeable getWriteable();
}
