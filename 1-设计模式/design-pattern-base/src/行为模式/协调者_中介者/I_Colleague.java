package 行为模式.协调者_中介者;

/**
 *  An abstract colleague
 */
public interface I_Colleague {
    public void Change();
    public void Action();
}