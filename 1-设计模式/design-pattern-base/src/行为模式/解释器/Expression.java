package 行为模式.解释器;

public abstract class Expression {
	public abstract int interpret(Context con);
}
